# views.py

from flask import render_template
from app import app

@app.route('/')
def home():
    title: str = 'Hello World!'
    return(render_template("home.j2"))

@app.route('/login')
def login():
    title = 'LogIn'
    return(render_template("login.j2", title=title))
