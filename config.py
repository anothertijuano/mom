# Statement for enabling the development enviroment
DEBUG = True

# Define the app directory
import os
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Protection agains CSRF
CSRF_ENABLED = True

# Secret key for data signing (this is a fake one)
CSRF_SESSION_KEY = "fakeKey"

# Secret key for signing cookies (this is a fake one)
SECRET_KEY = "fakeKey"
