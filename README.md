# Mom

**Who? What? When?** Mom is a project status and resource allocation tracking tool design to bring order to whatever your team is doing.

![Duck mom and his little ducks](https://media.giphy.com/media/8Bkqq19phBmRsZEVks/giphy.gif "Everyone needs a mom")

## Models

### Individual

The most critical element of the system.
- Name: str
- Email: str
- Location: str
- Title: Title
- Joining Date: Dates
- Leaving Date: Dates
- Status: {Active, Inactive, Unavailable} 

### Project

A set of deliverables that need to be completed between a time frame to reach a specific outcome.
- Name: str
- Team: Team
- Type: ProjectType
- Health: {Green, Amber, Red, Disabled}
- Completeness: float
- Location: str 
- StartDate: Dates
- FinishDate: Dates
- Deliverables: List[Deliverable]

### Deliverable

The primary element of a project, we can think of a project as a set of deliverables.
- Team: Team
- Type: Type
- Health: {Green, Amber, Red, Disabled}
- Completeness: float
- StartDate: Dates
- FinishDate: Dates
- Status: Status

### Team

This structure can be moddified according to the structure needed.
- Owner: Individual
- ...

### Dates

Dates is composed of the expected date and a list of revised dates, where the actual date is the last element of the list, the first element of RevisedDate is ExpectedDate.
- ExpectedDate: Date
- RevisedDate: List[Date]

### ProjectType

ProjectType is a list of posible types for a project, this needs to be change according to the team needs.
- ...

### DeliverableType

DeliverableType is a list of posible types for a project, this needs to be change according to the team needs. ProjectPlan is the basic deliverable type and reflects the status of the Project. 
- ProjectPlan


## Functionalities

We are using the MoSCoW method to describe the functionalities of the system. User permissions are base on a role-based access control (RBAC).

The user MUST CRUD employees  
The user MUST authenticate himself   
The user MUST CRUD projects  
The user MUST CRUD deliverables  
The user MUST associate deliverables to a project  
The user MUST open a resource requeriment in hours  
The user MUST register the % of time on each project  
The user MUST be able to see a Project Overview  
The user MUST be able to see a Project Summary  
The user MUST be able to see a Project Resource Details  
The user MUST be able to see the Resorce Weekly Distribution  
The user MUST be able to see the Allocation By Resource  
The user MUST be able to see Resource Weekly Availability In Percentage  
The user MUST be able to see the Weekly Availability Overview  
The user MUST be able to see the PTO Claims in %  
The user MUST be able to see the Project Progress and Deliverable Status  
The user MUST be able to see the Availability of the resource for the next 4 weeks  
The user MUST report the amount of time for his projects  
The user SHOULD be able to request PTO  
The user COULD send his report via mail  
The user COULD send his report via a chatbot 

